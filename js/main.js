var hwSlideSpeed = 700;
var hwTimeOut = 3000;
var hwNeedLinks = true;
 
$(document).ready(function() {

    $('.slide').css({
        "position" : "absolute",
        "top":'0',
        "left": '0'}).hide().eq(0).show();
    var slideNum = 0;
    var slideTime;
    slideCount = $("#slider .slide").size();
    var animSlide = function(arrow) {
        clearTimeout(slideTime);
        $('.slide').eq(slideNum).fadeOut(hwSlideSpeed);
        if(arrow == "next") {
            if(slideNum == (slideCount-1)) {slideNum=0;}
            else {slideNum++}
            }
        else if(arrow == "prew")
        {
            if(slideNum == 0) {slideNum=slideCount-1;}
            else {slideNum-=1}
        }
        else{
            slideNum = arrow;
        }
        $('.slide').eq(slideNum).fadeIn(hwSlideSpeed, rotator);
    }
    if(hwNeedLinks) {     
        $('.nextbutton').click(function(){
            animSlide("next");
            return false;
        });
        $('.prewbutton').click(function(){
            animSlide("prew");
            return false;
        });
    }
    var pause = false;
    var rotator = function() {
        if(!pause) {
            slideTime = setTimeout(function() {
                animSlide('next')
            }, hwTimeOut);
        }
    }
    $('#slider').hover(function(){ 
        clearTimeout(slideTime);
        pause = true;
    }, function(){
        pause = false;
        rotator();
    });
    rotator();

    $('.mainmenu').waypoint('sticky', {
        stuckClass: 'sticky',
        offset: 100,
    });

   $('section.gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
        }
    });
});